<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file is designed to be the view of the login
 */
ob_start();
$titre="Multiplix - Login";

?>
<div class="font-sans antialiased bg-grey-lightest">

    <!-- Content -->
    <form class="form" method="POST" action="index.php?action=login">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-5/6 max-w-lg mx-auto bg-purple-700 rounded shadow-2xl">

                    <img class="sm:object-contain sm:h-1/6 sm:w-full lg:object-contain lg:h-52 sm:w-full" src="img/multiplix_logo_big.png" />

                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Login to your account</div>
                    <div class="py-4 px-8">
                        <div class="flex mb-4"></div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Email Address</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-full" type="email" placeholder="Enter your email address" name="inputUserEmailAddress" required>
                        </div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">
                                Password
                            </div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-full" type="password" placeholder="Enter your password" name="inputUserPassword" required>
                            <?php if(isset($_SESSION["loginError"])){
                                echo "<p class='text-red-600 text-xs font-bold mt-1'>E-mail address or password are incorrect</p>";
                            }?>
                        </div>
                        <div class="flex items-center justify-between mt-8">
                            <button type="submit" class="bg-indigo-500 text-gray-100 p-4 w-full rounded-full tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-indigo-600
                                        shadow-lg">
                                Sign In
                            </button>
                        </div>
                        <div>
                            <p class="text-center my-4">
                                <a href="index.php?action=register" class="text-white font-bold text-sm no-underline hover:text-blue-300">Don't have an account ? Register now</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>



<?php
$contenu = ob_get_clean();
require "gabarit.php";


?>



