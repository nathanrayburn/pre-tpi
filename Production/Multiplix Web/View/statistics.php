<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file is the view of the statistics page
 */
ob_start();
$titre = "Multiplix - Statistics";

?>
<div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
    <div class="container mx-auto py-8">
        <div class="w-5/6 max-w-lg mx-auto bg-purple-700 rounded shadow-2xl pb-2">
            <div class="p-2 flex flex-row-reverse">
                <div class="m-2">
                    <a href="index.php?action=menu">
                        <button id="buttonExit" class="bg-red-600 text-gray-100 p-1.5 rounded-2xl tracking-wide
                                    font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-red-400
                                    shadow-lg items-center flex flex-row">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" height="30px">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                            <span>Exit</span>
                        </button>
                    </a>
                </div>
            </div>
            <img class="sm:object-contain sm:h-1/6 sm:w-full lg:object-contain lg:h-52 sm:w-full" src="img/multiplix_logo_big.png" />
            <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl lg:text-5xl xl:text-6xl">Statistics</div>
            <div class="flex flex-col">
                <div class="bg-pink-600 rounded-full m-2 text-center py-4 px-8 text-white font-mono font-bold text-2xl lg:text-3xl xl:text-4xl">Free mode</div>
                <div class="bg-blue-300 m-5 text-center rounded-3xl px-2 py-6 grid grid-cols-5">
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Table</p></div>
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Right</p></div>
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Wrong</p></div>
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Percent</p></div>
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Avg Time</p></div>
                    <?php
                    $array = $_SESSION["statsfreemode"];
                    foreach ($array as $result)
                    {
                        $mathtable = $result -> get_mathID();
                        $percent = $result -> get_percent();
                        $true = $result -> get_true();
                        $false = $result -> get_total() - $true;
                        $avg = round($result->get_avgReaction() / $result->get_total(),2);

                        echo '<div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base font-bold bg-blue-600  rounded-l-full">'.$mathtable.' </p></div> <div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base  font-blue bg-blue-600" >'.$true.'</p></div> <div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base font-bold bg-blue-600">'.$false.'</p></div> <div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base font-bold bg-blue-600">'.$percent.' % </p></div><div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base font-bold bg-blue-600 rounded-r-full">'.$avg.' s </p></div>';
                    }
                    ?>
                </div>
                <div class="bg-pink-600 rounded-full m-2 text-center py-4 px-8 text-white font-mono font-bold text-2xl lg:text-3xl xl:text-4xl">Guided mode</div>
                <div class="bg-blue-300 m-5 text-center rounded-3xl px-2 py-6 grid grid-cols-5">
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Table</p></div>
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Right</p></div>
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Wrong</p></div>
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Percent</p></div>
                    <div class="m-1"><p class="text-white font-bold text-xs lg:text-base xl:text-lg bg-blue-800 rounded-full">Avg Time</p></div>
                    <?php
                    $array = $_SESSION["statsguidedmode"];
                    foreach ($array as $result)
                    {
                        $mathtable = $result -> get_mathID();
                        $percent = $result -> get_percent();
                        $true = $result -> get_true();
                        $false = $result -> get_total() - $true;
                        $avg = round($result->get_avgReaction() / $result->get_total(),2);

                        echo '<div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base bg-blue-600  rounded-l-full">'.$mathtable.' </p></div> <div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base font-blue bg-blue-600" >'.$true.'</p></div> <div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base font-bold bg-blue-600">'.$false.'</p></div> <div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base font-bold bg-blue-600">'.$percent.' % </p></div><div class="py-1"><p class="text-white text-xs lg:text-sm xl:text-base font-bold bg-blue-600 rounded-r-full">'.$avg.' s </p></div>';
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>
</div>

<?php

$contenu = ob_get_clean();
require "gabarit.php";

?>


