<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file is the view of the options page
 */
ob_start();
$titre="Multiplix - Options";
?>
<div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
    <div class="container mx-auto py-8">
        <div class="w-5/6 max-w-lg mx-auto bg-purple-700 rounded shadow-2xl pb-2">
            <div class="p-2 flex flex-row-reverse">
                <div class="m-2">
                    <a href="index.php?action=menu">
                        <button id="buttonExit" class="bg-red-600 text-gray-100 p-1.5 rounded-2xl tracking-wide
                                    font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-red-400
                                    shadow-lg items-center flex flex-row">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" height="30px">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                            <span>Exit</span>
                        </button>
                    </a>
                </div>
            </div>
            <div>
                <img class="sm:object-contain sm:h-1/6 sm:w-full lg:object-contain lg:h-52 sm:w-full" src="img/multiplix_logo_big.png" />
            </div>
            <form class="form" method="POST" action="index.php?action=option">
                <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl lg:text-5xl xl:text-6xl" id="textPlay">What do you want to learn ?</div>
                <div class="m-2 content-center">
                    <div class="grid grid-cols-7 grid-rows-2 gap-0.5 mb-2">
                        <label class="items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox0" class="form-checkbox h-5 w-5 text-gray-600 rounded" ><span class="ml-2 text-white">0</span>
                        </label>

                        <label class="items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox1" class="form-checkbox h-5 w-5 text-red-600 rounded" ><span class="ml-2 text-white">1</span>
                        </label>

                        <label class="items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox2" class="form-checkbox h-5 w-5 text-orange-600 rounded" ><span class="ml-2 text-white">2</span>
                        </label>

                        <label class="items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox3" class="form-checkbox h-5 w-5 text-yellow-600 rounded" ><span class="ml-2 text-white">3</span>
                        </label>

                        <label class="items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox4" class="form-checkbox h-5 w-5 text-green-600 rounded" ><span class="ml-2 text-white">4</span>
                        </label>

                        <label class="items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox5" class="form-checkbox h-5 w-5 text-teal-600 rounded" ><span class="ml-2 text-white">5</span>
                        </label>

                        <label class="items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox6" class="form-checkbox h-5 w-5 text-blue-600 rounded" ><span class="ml-2 text-white">6</span>
                        </label>
                        <label class="items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox7" class="form-checkbox h-5 w-5 text-gray-600 rounded" ><span class="ml-2 text-white">7</span>
                        </label>

                        <label class="items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox8" class="form-checkbox h-5 w-5 text-red-600 rounded" ><span class="ml-2 text-white">8</span>
                        </label>

                        <label class="items-center items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox9" class="form-checkbox h-5 w-5 text-orange-600 rounded" ><span class="ml-2 text-white">9</span>
                        </label>

                        <label class="items-center items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox10" class="form-checkbox h-5 w-5 text-yellow-600 rounded" ><span class="ml-2 text-white">10</span>
                        </label>

                        <label class="items-center items-center mt-3 mx-1 sm:mx-0.5">
                            <input type="checkbox" name="checkbox11" class="form-checkbox h-5 w-5 text-green-600 rounded" ><span class="ml-2 text-white">11</span>
                        </label>

                        <label class="items-center items-center mt-3 mx-1">
                            <input type="checkbox" name="checkbox12" class="form-checkbox h-5 w-5 text-teal-600 rounded" ><span class="ml-2 text-white">12</span>
                        </label>
                    </div>


                </div>
                <?php
                if(isset($_SESSION["optionError"])){
                    echo '
                        <div>
                            <p class="text-center text-red-600 font-bold text-xs">Need to choose at least two tables !</p>
                        </div>';
                }
                ?>
                <div class="m-2">
                    <a href="index.php?action=menu">
                        <button type="submit" class="bg-green-600 text-gray-100 p-4 w-full rounded-full tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-400
                                        shadow-lg">
                            OK
                        </button>
                    </a>
                </div>
            </form>
        </div>

    </div>
</div>

<?php
$contenu = ob_get_clean();
require "gabarit.php";


?>


