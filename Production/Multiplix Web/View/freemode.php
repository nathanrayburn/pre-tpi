<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file is the view of the free mode game
 */
ob_start();
$titre="Multiplix - Free mode";
?>
<script src="../js/freemode.js"></script>
<div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
    <div class="container mx-auto py-8">
        <div class="w-5/6 max-h-screen max-w-lg mx-auto bg-purple-700 rounded shadow-2xl pb-2">
               <div class="p-2 flex flex-row-reverse">
                    <div class="m-2">
                        <a href="index.php?action=menu">
                            <button id="buttonExit" class="bg-red-600 text-gray-100 p-1.5 rounded-2xl tracking-wide
                                    font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-red-400
                                    shadow-lg items-center flex flex-row">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" height="30px">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                <span>Exit</span>
                            </button>
                        </a>
                    </div>
                    <div class="m-2">
                        <a>
                            <button id="userStatistics" class="bg-green-500 text-gray-100 p-1.5 rounded-2xl tracking-wide
                                    font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-300
                                    shadow-lg items-center flex flex-row">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" height="30px">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                </svg>
                                <span>My statistics</span>
                            </button>
                        </a>
                    </div>
               </div>

            <div>
                <img id="img" class="sm:object-contain sm:h-1/6 sm:w-full lg:object-contain lg:h-52 sm:w-full" src="img/multiplix_logo_big.png"/>
            </div>
            <div class="m-2">
                <h2 class="bg-pink-600 rounded-full text-center py-4 px-8 text-white font-mono font-bold text-3xl lg:text-5xl xl:text-6xl">Free Mode</h2>
            </div>
            <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl lg:text-5xl xl:text-6xl" id="textPlay">Play a new game</div>
            <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl lg:text-5xl xl:text-6xl" style="display:none" id="textQuestion">What is ..</div>
            <div class="m-2">
                    <button type="submit" id="startButton" class="bg-green-600 text-gray-100 p-4 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-400
                                shadow-lg">
                        Click to start
                    </button>
            </div>
            <div class="m-2 text-center">
                <h2 class="text-4xl lg:text-6xl font-mono font-bold text-white bg-yellow-600 rounded-3xl" id="gameText" style="display:none">8 x 9 = ?</h2>
            </div>
            <div class="m-2 text-center">
                <h2 class="text-4xl lg:text-6xl font-mono font-bold text-white bg-red-700 rounded-3xl" id="safeTimerDisplay" style="display:none">00</h2>
            </div>
            <div class="m-2 grid gap-4 grid-cols-2 place-items-center">
                <button id="imageBravo" class="bg-green-600 text-gray-100 p-4 w-4/5 rounded-3xl tracking-wide
                                font-semibold font-display
                                shadow-lg" style="display: none">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z" />
                    </svg>
                    <span>Correct Answer</span>
                </button>
                <button id="imageWrong" class="bg-red-600 text-gray-100 p-4 w-4/5 rounded-3xl tracking-wide
                                font-semibold font-display
                                shadow-lg" style="display: none">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636" />
                    </svg>
                    <span>Wrong Answer</span>
                </button>
                <button id="buttonNext" class="bg-yellow-500 text-gray-100 p-4 w-4/5 rounded-3xl tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-yellow-400
                                shadow-lg" style="display: none">                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-8.707l-3-3a1 1 0 00-1.414 1.414L10.586 9H7a1 1 0 100 2h3.586l-1.293 1.293a1 1 0 101.414 1.414l3-3a1 1 0 000-1.414z" clip-rule="evenodd" />
                    </svg>
                    <span>Next</span>
                </button>
            </div>
           <div class="m-2 grid gap-4 grid-cols-2">
                <div class="">
                    <button type="submit" id="button1" class="bg-yellow-400 text-gray-100 p-8 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-yellow-600
                                shadow-lg" style="display:none">
                        72
                    </button>
                </div>
                <div class="">
                    <button type="submit" id="button2" class="bg-blue-800 text-gray-100 p-8 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-blue-500
                                shadow-lg" style="display:none">
                        84
                    </button>
                </div>
                <div class="">
                    <button type="submit" id="button3" class="bg-pink-600 text-gray-100 p-8 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-pink-700
                                shadow-lg" style="display:none">
                        12
                    </button>
                </div>
                <div class="">
                    <button type="submit" id="button4" class="bg-green-500 text-gray-100 p-8 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-900
                                shadow-lg" style="display:none">
                        56
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
?>


