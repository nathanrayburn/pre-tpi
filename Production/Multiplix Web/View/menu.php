<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file is the view of the menu page
 */
ob_start();
$titre="Multiplix - Menu";

?>
<script src="../js/menu.js"></script>
<div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
    <div class="container mx-auto py-8">
        <div class="w-5/6 max-w-lg mx-auto bg-purple-700 rounded shadow-2xl pb-2">
            <img class="sm:object-contain sm:h-1/6 sm:w-full lg:object-contain lg:h-52 sm:w-full" src="img/multiplix_logo_big.png" />
            <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl lg:text-5xl xl:text-6xl">Play now</div>
            <div class="m-2">
                <a href="index.php?action=freemode">
                    <button type="submit" class="bg-green-400 text-gray-100 p-4 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-600
                                shadow-lg">
                        Free mode
                    </button>
                </a>
            </div>
            <div class="m-2">
                <a href="index.php?action=guidedmode">
                    <button class="bg-yellow-400 text-gray-100 p-4 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-yellow-600
                                shadow-lg">
                        5 second mode
                    </button>
                </a>
            </div>
            <div class="m-2">
                <a href="index.php?action=option">
                    <button id="buttonOption" class="bg-pink-400 text-gray-100 p-4 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-pink-700
                                shadow-lg">
                        Options
                    </button>
                </a>
            </div>
            <div class="m-2">
                <a>
                    <button id="userStatistics" class="bg-blue-400 text-gray-100 p-4 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-blue-600
                                shadow-lg">
                        Statistics
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
<?php
$contenu = ob_get_clean();
require "gabarit.php";


?>


