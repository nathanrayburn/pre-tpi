<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file is the body of all view pages
 */
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title><?=$titre;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Html5TemplatesDreamweaver.com">
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> <!-- Remove this Robots Meta Tag, to allow indexing of site -->

    <link href="css/tailwind.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>


</head>
<body id="pageBody" class="bg-blue-500">
<div>
    <div id="background-wrap">
        <div class="x2">
            <div class="cloud"></div>
        </div>
        <div class="x8">
            <div class="cloud"></div>
        </div>
        <div class="x3">
            <div class="cloud"></div>
        </div>
        <div class="x1">
            <div class="cloud"></div>
        </div>
        <div class="x4">
            <div class="cloud"></div>
        </div>
        <div class="x9">
            <div class="cloud"></div>
        </div>
        <div class="x5">
            <div class="cloud"></div>
        </div>
        <div class="x10">
            <div class="cloud"></div>
        </div>
        <div class="x6">
            <div class="cloud"></div>
        </div>
    </div>
</div>
<!-- Top Nav -->
<div class="w-full bg-purple-700 fixed shadow-2xl z-1">
    <div class="container mx-auto">
        <div class="w-full flex justify-between items-center py-4 px-8">

            <!-- Brand -->
            <div class="text-center text-white font-bold">Multiplix Games</div>
            <!-- Navigation -->
            <div class="">
                <?php if(isset($_GET['action'])) { $action = $_GET['action'];}else{$action='home';} ?>
                <?php if (isset($_SESSION["userEmail"])) :?>
                    <a id="buttonLogout" class="text-base bg-white hover:bg-purple-500 hover:text-white text-black font-bold border border-black hover:border-white py-2 px-4 rounded-full" href="index.php?action=logout">
                        Logout
                    </a>
                <?php endif;?>
            </div>

        </div>
    </div>
</div>
<div class="">

    <div class="divPanel notop page-content">
        <div class="row-fluid">

            <!--__________CONTENU__________-->

            <div class="" id="divMain">
                <?=$contenu;?>
            </div>

            <!--________FIN CONTENU________-->

        </div>

        <div id="footerInnerSeparator"></div>
    </div>
</div>

<!-- Footer -->
<footer class="w-full bg-grey-lighter py-8">
    <div class="container mx-auto text-center px-8">
        <p class="text-white underline mb-2 text-sm">This is a product of <span class="font-bold">Multiplix Games</span></p>
    </div>
</footer>

</body>
</html>