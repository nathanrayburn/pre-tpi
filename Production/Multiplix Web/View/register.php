<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file is the view of the register page
 */
ob_start();
$titre="Multiplix - Register";

?>

<div class="font-sans antialiased bg-grey-lightest">
    <!-- Content -->
    <form class = "" method="POST" action="index.php?action=register">
        <div class="w-full bg-grey-lightest" style="padding-top: 4rem;">
            <div class="container mx-auto py-8">
                <div class="w-5/6 max-w-lg mx-auto bg-purple-700 rounded shadow-2xl">
                    <img class="sm:object-contain sm:h-1/6 sm:w-full lg:object-contain lg:h-52 sm:w-full" src="img/multiplix_logo_big.png" />
                    <div class="text-center py-4 px-8 text-white font-mono font-bold text-3xl">Register now</div>
                    <div class="py-4 px-8">
                        <div class="flex mb-4"></div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">Email Address</div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-full" type="email" placeholder="Enter your email address" name="registerUserEmailAddress">
                        </div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">
                                Password
                            </div>
                            <input class="w-full text-lg px-6 py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500  border-4 rounded-full" type="password" placeholder="Enter your password" name="registerUserPassword">
                        </div>
                        <div class="mb-4">
                            <div class="text-sm font-bold text-gray-100 tracking-wide">
                                Confirm password
                            </div>
                            <input class="w-full text-lg px-6 py-2  border-b border-gray-300 focus:outline-none focus:border-yellow-500 border-4 rounded-full" type="password" placeholder="Confirm your password" name="registerRepeatUserPassword">
                            <?php if(isset($_SESSION["registerError"])){
                                echo "<p class='text-red-600 text-xs font-bold mt-1'>Email already exists or passwords are entered incorrectly</p>";
                            }?>
                        </div>
                        <div class="flex items-center justify-between mt-8">
                            <button type="submit" class="bg-indigo-500 text-gray-100 p-4 w-full rounded-full tracking-wide
                                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-indigo-600
                                        shadow-lg">
                                Sign up
                            </button>
                        </div>
                        <div>
                            <p class="text-center my-4">
                                <a href="index.php?action=login" class="text-white font-bold text-sm no-underline hover:text-blue-300">I already have an account? Sign in</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<?php
$contenu = ob_get_clean();
require "gabarit.php";

?>



