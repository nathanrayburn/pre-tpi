/**
 * Author : Nathan Rayburn
 * Date : 3/18/21
 * Description : This script is designed to run a game of math tables multiplication which has a 4 question system.
 */

//This class is to create objects with properties of a question
class Question {
    constructor(table1,table2,mathID,answer,questionLoadTime) {
        this._table1 = table1;
        this._table2 = table2;
        this._mathID = mathID;
        this._answer = answer;
        this._questionLoadTime = questionLoadTime;
    }
    /**
     * Accessors for _questionLoadTime
     * get QuestionLoadTime : return date
     * set QuestionLoadTime : Para date
     */
    set QuestionLoadTime(createdTime)
    {
        this._questionLoadTime = createdTime;
    }
    get QuestionLoadTime()
    {
        return this._questionLoadTime;
    }
    /**
     * Accessors for _table1
     * get Table1 : return int
     * set Table1 : Para int
     */
    set Table1(newTable1)
    {
        this._table1 = newTable1;
    }
    get Table1()
    {
        return this._table1;
    }
    /**
     * Accessors for _table2
     * get Table2 : return int
     * set Table2 : Para int
     */
    set Table2(newTable2)
    {
        this._table2 = newTable2;
    }

    get Table2()
    {
        return this._table2;
    }
    /**
     * Accessors for _mathID
     * get MathID : return int
     * set MathID : Para int
     */
    set MathID(newMathID)
    {
        this._mathID = newMathID;
    }

    get MathID()
    {
        return this._mathID;
    }
    /**
     * Accessors for _answer
     * get Answer : return int
     * set Answer : Para int
     */
    set Answer(newAnswer)
    {
        this._answer = newAnswer;
    }

    get Answer()
    {
        return this._answer;
    }
    /**
     * This function is designed to return directly the question with a format A X B
     * return string
     */
    getQuestion()
    {
        return this._table1 + " x " + this._table2;
    }
    /**
     * This function is designed to return directly the answer of the questtion with a format
     * return string
     */
    getAnswer()
    {
        return "The answer was " + this._answer;
    }
}
//This class is designed to create a virtual button with values to add for each html button
class AnswerButton {

    constructor(buttonNumber,buttonValue) {
        this._buttonNumber = buttonNumber; //Button ID, goes from 1 to 4. To assign buttons virtually with the ones in the HTML
        this._buttonValue = buttonValue; //This variable contains the value of a random answer or the actual answer of the question
    }

    /**
     * Accessors for _buttonNumber
     * get ButtonNumber : return int
     * set ButtonNumber : Para int
     */
    set ButtonNumber(newButtonNumber)
    {
        this._buttonNumber = newButtonNumber;
    }
    get ButtonNumber()
    {
        return this._buttonNumber;
    }
    /**
     * Accessors for _buttonValue
     * get ButtonValue : return int
     * set ButtonValue : Para int
     */
    set ButtonValue(newButtonValue)
    {
        this._buttonValue = newButtonValue;
    }

    get ButtonValue()
    {
        return this._buttonValue;
    }
}

/**
 * This function is activated when the document is loading
 */
$(document).ready(function () {
    //Initializing the number of questions
    let numberofquestions = 24;
    //call the startup function
    let questions = main(numberofquestions);
    let currentQuestion = 1;
    buttonTable = LoadGame(questions,currentQuestion);

    questions[currentQuestion].QuestionLoadTime = Date.now();     //set the time of the initial question

    //On click of the start button -> Change the format when the button is clicked
    $("#startButton").click(function () {
        $("#startButton").hide("fast", "linear")
        $("#textPlay").hide("fast", "linear")
        $("#img").hide("fast","linear")
        $("#textQuestion").css("display", "block")
        $("#gameText").css("display", "block")
        $("#button1").css("display", "block")
        $("#button2").css("display", "block")
        $("#button3").css("display", "block")
        $("#button4").css("display", "block")
        $("#safeTimerDisplay").css("display","none");
    })
    /**
     * On "button1" click, the function will get the buttons virtual value and check if it corresponds to the answer of the question and send a bool to AnswerResult
     * The function will increment the current question variable to go to the next question
     * Type void
     */
    $("#button1").click(function () {

        let tempButtonValue = GetButtonValueByID(1); //Get the answer value from the button's object
        //Check's the table value and checks if the answer is correct.
        if(tempButtonValue === questions[currentQuestion].Answer)
        {
            AnswerResult(questions,currentQuestion,true); //Update results on right answer
        }else{
            AnswerResult(questions,currentQuestion,false); //update results on false answer
        }
        //Continue going through the questions if the list hasn't finished
        if(questions.length-1 >= currentQuestion)
        {
            currentQuestion++; //Set the next question
        }
    })
    /**
     * On "button2" click, the function will get the buttons virtual value and check if it corresponds to the answer of the question and send a bool to AnswerResult
     * The function will increment the current question variable to go to the next question
     * Type void
     */
    $("#button2").click(function () {
        let tempButtonValue = GetButtonValueByID(2); //Get the answer value from the button's object
        //Check's the table value and checks if the answer is correct.
        if(tempButtonValue === questions[currentQuestion].Answer)
        {
            AnswerResult(questions,currentQuestion,true); //Update results on right answer
        }else{
            AnswerResult(questions,currentQuestion,false);//update results on false answer
        }
        //Continue going through the questions if the list hasn't finished
        if(questions.length-1 >= currentQuestion)
        {
            currentQuestion++;
        }
    })
    /**
     * On "button3" click, the function will get the buttons virtual value and check if it corresponds to the answer of the question and send a bool to AnswerResult
     * The function will increment the current question variable to go to the next question
     * Type void
     */
    $("#button3").click(function () {
        let tempButtonValue = GetButtonValueByID(3); //Get the answer value from the button's object
        //Check's the table value and checks if the answer is correct.
        if(tempButtonValue === questions[currentQuestion].Answer)
        {
            AnswerResult(questions,currentQuestion,true); //Update results on right answer
        }else{
            AnswerResult(questions,currentQuestion,false); //update results on false answer
        }
        //Continue going through the questions if the list hasn't finished
        if(questions.length-1 >= currentQuestion)
        {
            currentQuestion++;
        }
    })
    /**
     * On "button4" click, the function will get the buttons virtual value and check if it corresponds to the answer of the question and send a bool to AnswerResult
     * The function will increment the current question variable to go to the next question
     * Type void
     */
    $("#button4").click(function () {
        let tempButtonValue = GetButtonValueByID(4);  //Get the answer value from the button's object
        //Check's the table value and checks if the answer is correct.
        if(tempButtonValue === questions[currentQuestion].Answer)
        {
            AnswerResult(questions,currentQuestion,true); //Update results on right answer
        }else{
            AnswerResult(questions,currentQuestion,false); //update results on false answer
        }
        if(questions.length-1 >= currentQuestion)         //Continue going through the questions if the list hasn't finished
        {
            currentQuestion++;
        }
    })
    /**
     * On "buttonNext" click, the function will load the new questions and if all questions have been answered, sends the user to home.
     * Type void
     */
    $("#buttonNext").click(function () {

        if(currentQuestion < questions.length)        //Load the game if the questions haven't all been played
        {
            buttonTable = LoadGame(questions,currentQuestion); //Set the new answers to the button's table
            //set the time of the current question
            questions[currentQuestion].QuestionLoadTime = Date.now(); //set the new date of the loaded question
            LoadNewQuestion(); //Reformat the page
            //check if the next question is last
            if(currentQuestion + 1 === questions.length)
            {
                    $("#buttonNext").text("Finish the game.");
            }
        }else
        {
            ReturnToMenu(); //return to menu
        }

    })
    /**
     * On "userStatistics" click, the function will open a new tab to the statistic's page
     * Type void
     */
    $("#userStatistics").click(function()
    {
        var url= document.URL; //Get the documents url
        var split = url.split("action=");//Seperate the url
        var playerEmail = getCookie("splitEmail") + "@" + getCookie("splitDomain"); //get user's email from cookies
        var newUrl = split[0] + "action=statistics&"+"email="+playerEmail; //set the a new $_Get in the url
        window.open(newUrl); //Load the new URL in an other tab
    })

    /**
     * On "buttonExit" click, the function will remove the current game's cookie
     * Type void
     */
    $("#buttonExit").click(function (){
            removeCookie("freemode");
    })
});
//This function is designed to end the game and send back to menu
/**
 * This function is designed to redirect the current URl to the menu
 * Type void
 */
function ReturnToMenu()
{
  var url= document.URL; //Get the documents url
  var split = url.split("action="); //Seperate the url
  var newUrl = split[0] + "action=menu"; //set the a new $_Get in the url
  location.replace(newUrl); //Replace the current url to the new one
}
//this function is designed to reformat the page
/**
 * This function is designed to format the page of the game
 * Type void
 */
function LoadNewQuestion()
{
    $("#button1").css("display", "block");
    $("#button2").css("display", "block");
    $("#button3").css("display", "block");
    $("#button4").css("display", "block");
    $("#imageBravo").css("display","none")
    $("#imageWrong").css("display","none");
    $("#buttonNext").css("display","none");
    $("#textQuestion").css("display","block");
    $("#safeTimerDisplay").css("display","none");
}
/**
 * This function is designed to get the button's value (answer)
 * Para @buttonID : int
 * Type void
 */
function GetButtonValueByID(buttonID)
{
    for (var i=0; i < buttonTable.length; i++) {
        if (buttonTable[i].ButtonNumber === buttonID) {
            return buttonTable[i].ButtonValue;
        }
    }
}
//Function is designed to call the function to update data depending on answer result true or false
/**
 * This function is designed to update data depending on the answer result bool
 */
function AnswerResult(questions,questionIndex,truth)
{
    let reaction = GetReactionTime(questions[questionIndex].QuestionLoadTime); //The user's reaction time
    //Page format
    $("#button1").css("display", "none");
    $("#button2").css("display", "none");
    $("#button3").css("display", "none");
    $("#button4").css("display", "none");
    $("#buttonNext").css("display","block");
    $("#textQuestion").css("display","none");
    $("#safeTimerDisplay").css("display","block");
    $("#gameText").text(questions[questionIndex].getAnswer()); //Display the answer
    document.getElementById('safeTimerDisplay').innerHTML= "Your reaction time is " + reaction + "'s"; //display reaction time

    switch(truth){
        case true:
        //Reformat page on true
        $("#imageBravo").css("display","block");
        UpdateRecord(questions[questionIndex].MathID,truth,reaction); //Updates the result to data base
        break;
        case false:
        //Reformat page on false
        $("#imageWrong").css("display","block");
        UpdateRecord(questions[questionIndex].MathID,truth,reaction);//Updates the result to data base
        break;
        default:
            break;
    }
}
/**
 * This function is designed to calculate the reaction time
 * Para @createdTime : Date
 * return float
 * */
function GetReactionTime(createdTime)
{
    clickedTime=Date.now();
    reactionTime=(clickedTime-createdTime)/1000;
    return reactionTime;
}

/**
 * This function is designed to update the question's result to data base by going through PHP in POST
 * para @id : int, @truth : bool, @react : float
 */
function UpdateRecord(id,truth,react)
{
    var playerEmail = getCookie("splitEmail") + "@" + getCookie("splitDomain"); //Gets player's email
    //Open in background update.php and send data by post
    jQuery.ajax({
        type: "GET",
        url: "index.php",
        data:{
            action:"update",
            tableID:id,
            answer: truth,
            gameMode:"freemode",
            reactionTime:react,
            email:playerEmail,
        },
        cache: false,
        success: function(response)
        {
            console.log("The result has been saved to data base.");
        }
    });
}
/**
 * This function is designed to get a cookie by it's name
 * Para @name : string
 */
function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
/**
 * This function is designed to create an array of string that was from value that was from a cookie
 * Para @resultfromcookie : string
 * return res, array [string]
 */
function createArrayFromCookie(options) {
    return options.split('_');
}
/**
 * This function is the main function to call all functions that are required to start the game and returns the created questions
 * Para @numberofquestions : int
 * return array [Question]
 */
function main(numberofquestions) {
    let name = "freemode"; //gamemode
    let resultfromcookie = getCookie(name); //Get the selected check boxes from cookie

    let listcheckedboxes = createArrayFromCookie(resultfromcookie);
    let mathtables = listcheckedboxes.map((checkbox) => extractTableNumberFromList(checkbox))

    return createQuestions(mathtables,numberofquestions);
}
/**
 * This function is designed to extract number values from a string
 * Para @item : string
 * return int
 */
function extractTableNumberFromList(item) {
    return parseInt(item.match(/\d+/)[0]); //return the number that was in the string
}
/**
 * This function is designed to create questions in function of if the questions have to be made equally or not
 * Para @mathTables : array int, numberofquestions : int
 * return array [Question]
 */
function createQuestions(mathTables,numberofquestions) {
    var check = moduloCheck(mathTables,numberofquestions);
    var eqquestions = Array;
    switch (check) {
        case true:
            eqquestions = equalQuestions(mathTables,numberofquestions)
            break;
        case false:
            eqquestions = inequalquestions(mathTables,numberofquestions);
            break;
        default:
            break;
    }
    return eqquestions;
}
/**
 * This function is designed to get random questions and equally made questions to concat both lists and return all questions
 * Para @mathTables : list int, @numberofquestions : int
 * return array [Question]
 */
function inequalquestions(mathTables,numberofquestions)
{
    const remainder = moduloRemainder(numberofquestions,mathTables.length) //get the rest of the modulo
    const remained = numberofquestions-remainder;  //calculate the number of questions that need to be made equally
    const eqquestions = equalQuestions(mathTables,remained) //get equally made questions
    let otherquestions = randomQuestions(mathTables, remainder); //get random list of questions

    return eqquestions.concat(otherquestions) //Concat both lists of random questions and questions that are equally made

}
/**
 * This function is designed to return the rest if the modulo isn't 0
 * Para @numberofquestions : int, optionsLength : int
 * return int
 */
function moduloRemainder(numberofquestions,optionsLength)
{
    return numberofquestions % optionsLength;
}
/**
 * This function is designed to make generate questions equally for each math table
 * Para @mathTables : list int, @numberofquestions : int
 * return @listofquestions, array [Question]
 */
function equalQuestions(mathTables,numberofquestions) {
    //Initializing variables
    let i = 0;
    let b = 0;
    let listofquestions = Array(null); //list of questions that will be made
    let  randomNumber = Math.floor(Math.random() * 13); //Random number
    let randomNumberIndex = [];
    const questionspertable = numberofquestions / mathTables.length; //calculates the number of questions per table
    //Create brut questions
    for (b = 1; b <= mathTables.length; b++) {
        for (i = 0; i < questionspertable; i++) {
            //Make random numbers and make sure that it doesn't already exist
            do
            {
                randomNumber = Math.floor(Math.random() * 13);
            }while(randomNumberIndex.includes(randomNumber) === true || (randomNumber === 0 && mathTables[b-1] === 0));
                randomNumberIndex.push(randomNumber); //adds the random number to an archive
                let tempindexvalue = mathTables[b-1];
                let answer = tempindexvalue * randomNumber;
                let tempQuestion = new Question();
                //set the question's properties
                tempQuestion.Table1 = tempindexvalue;
                tempQuestion.Table2 = randomNumber;
                tempQuestion.MathID = tempindexvalue;
                tempQuestion.Answer = answer;

                listofquestions.push(tempQuestion); //add the question to the list
        }
        randomNumberIndex = []; //Reset index values
    }
    return listofquestions; //return list of questions
}
/**
 * this function is designed to create random answers
 * Para @mathtables : list string, @remainder : int
 * return @listofquestions, array [Question]
 */
function randomQuestions(mathTables,remainder)
{
    //Initializing variables
    var i = 0;
    var randomNumber = Math.floor(Math.random() * 13);
    var randomIndex = Math.floor(Math.random() * remainder);
    var questionAlreadyUsed = [];
    var listofquestions = [];

    //Generate the questions
    for(i=1;i<=remainder;i++)
    {
        //Make random numbers and make sure that it doesn't already exist
        do{
            randomNumber = Math.floor(Math.random() * 13);//create random number for the question
            randomIndex = Math.floor(Math.random() * remainder);//create random number for the index of numbers in math tables
        }while(questionAlreadyUsed.includes(mathTables[randomIndex]) === true);
        questionAlreadyUsed.push(mathTables[randomIndex]);//Archive the random number
        let tempQuestion = new Question(); //Instance a question
        //Set question's properties
        tempQuestion.Table1 = mathTables[randomIndex];
        tempQuestion.Table2 = randomNumber;
        tempQuestion.MathID = mathTables[randomIndex];
        tempQuestion.Answer = mathTables[randomIndex] * randomNumber;

        listofquestions.push(tempQuestion); //add question to list
    }

    return listofquestions;  //return list of questions
}
/**
 * This function is designed to check if the modulo of the number of questions by math tables is 0
 * para @mathtables : array int, @numberofquestions : int
 * return @truth, bool
 */
function moduloCheck(options,numberofquestions) {
    var truth = false;
    var length = options.length;
    if ((numberofquestions % length) === 0) {
        truth = true;
    }
    return truth;
}

/**
 * This function is designed to create a virtual button array so we can know which button corresponds to which value
 * This function also updates the text of the buttons directly when inserting value to the virtual button
 * Para @questions : array [Question], @currentquestion : int
 * return array [AnswerButton]
 */
function LoadGame(questions,currentquestion)
{
    $("#gameText").text(questions[currentquestion].getQuestion()); //Update the question in the view
    answers = randomAnswers(questions[currentquestion]); //Generate random answers for 3 buttons
    randomGeneration = Math.floor(Math.random() * 4);
    //Instance of buttons
    let buttonAnswer1 = new AnswerButton();
    let buttonAnswer2 = new AnswerButton();
    let buttonAnswer3 = new AnswerButton();
    let buttonAnswer4 = new AnswerButton();
    let buttonAnswerList = [];

    //Setting each button a property
    buttonAnswer1.ButtonNumber = 1;
    buttonAnswer2.ButtonNumber = 2;
    buttonAnswer3.ButtonNumber = 3;
    buttonAnswer4.ButtonNumber = 4;

    //Setting randomly the answers in the buttons and changing the button's text value
    switch(randomGeneration)
    {
        case 0 :
            $("#button1").text(answers[0])

            buttonAnswer1.ButtonValue = answers[0];

            $("#button2").text(answers[1]);

            buttonAnswer2.ButtonValue = answers[1];

            $("#button3").text(answers[2]);

            buttonAnswer3.ButtonValue = answers[2];

            $("#button4").text(answers[3]);

            buttonAnswer4.ButtonValue = answers[3];
            buttonAnswerList.push(buttonAnswer1,buttonAnswer2,buttonAnswer3,buttonAnswer4);
            break;
        case 1 :
            $("#button1").text(answers[2]);

            buttonAnswer1.ButtonValue = answers[2];

            $("#button2").text(answers[0]);

            buttonAnswer2.ButtonValue = answers[0];

            $("#button3").text(answers[3]);

            buttonAnswer3.ButtonValue = answers[3];

            $("#button4").text(answers[1])

            buttonAnswer4.ButtonValue = answers[1];
            buttonAnswerList.push(buttonAnswer1,buttonAnswer2,buttonAnswer3,buttonAnswer4);
            break;
        case 2 :
            $("#button1").text(answers[1]);

            buttonAnswer1.ButtonValue = answers[1];

            $("#button2").text(answers[3]);

            buttonAnswer2.ButtonValue = answers[3];

            $("#button3").text(answers[0]);

            buttonAnswer3.ButtonValue = answers[0];

            $("#button4").text(answers[2]);

            buttonAnswer4.ButtonValue = answers[2];
            buttonAnswerList.push(buttonAnswer1,buttonAnswer2,buttonAnswer3,buttonAnswer4);
            break;
        case 3 :
            $("#button1").text(answers[3]);

            buttonAnswer1.ButtonValue = answers[3];

            $("#button2").text(answers[2]);

            buttonAnswer2.ButtonValue = answers[2];

            $("#button3").text(answers[1]);

            buttonAnswer3.ButtonValue = answers[1];

            $("#button4").text(answers[0]);

            buttonAnswer4.ButtonValue = answers[0];

            buttonAnswerList.push(buttonAnswer1,buttonAnswer2,buttonAnswer3,buttonAnswer4);
            break;
        default :
            break;
    }
    return buttonAnswerList; //returns a table of button objects
}

/**
 * This function is designed to create random answer for the 3 of the buttons out of 4
 * Para @question : [Question]
 * return array int
 */
function randomAnswers(question)
{
    //Initializing variables
    var answerList = [];
    var randomNumberIndex =[];
    var randomNumber;
    //Create 4 random answers
    for(i = 0;i<3;i++)
    {

        if(parseInt(question.MathID) !== 0)
        {
            //Create answers without doing duplicates
            do
            {
                randomNumber = Math.floor(Math.random() * 13);
            }while(randomNumberIndex.includes(randomNumber) === true || question.Table2 === randomNumber);
            randomNumberIndex.push(randomNumber);
            tempAnswer = question._mathID * randomNumber;
            answerList.push(tempAnswer);
        }else
        {
            //Create answers without doing duplicates
            do
            {
                randomNumber = Math.floor(Math.random() * 13);
            }while(randomNumberIndex.includes(randomNumber) === true || question.Table1 === randomNumber);
            randomNumberIndex.push(randomNumber);
            tempAnswer = question.Table2 * randomNumber;
            answerList.push(tempAnswer);
        }
    }
    answerList.push(question.Answer);
    return answerList; //return the list of answers
}
/**
 * This function is designed to create a cookie
 * Para @name : string, @value : string, @days : int
 * Type void
 */
function setCookie(name, value, days) {
    var d = new Date;
    d.setTime(d.getTime() + 24*60*60*1000*days);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}
/**
 * This function is designed to remove a cookie by name
 * para @name, string
 * Type void
 */
function removeCookie(name) { setCookie(name, '', -1); }