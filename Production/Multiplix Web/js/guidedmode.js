/**
 * Author : Nathan Rayburn
 * Date : 3/18/21
 * Description : This script is designed to run a game of math tables of multiplication which has a 5 second timer.
 */

//This class is to create objects with properties of a question
class Question {
    constructor(table1,table2,mathID,answer,questionLoadTime,reactionTime) {
        this._table1 = table1; //The multiplier
        this._table2 = table2; //The multiplicative
        this._mathID = mathID; //The reference to know the which table the user is playing
        this._answer = answer; //Answer of the calc
        this._questionLoadTime = questionLoadTime; //Load time of the question
        this._reactionTime = reactionTime;
    }


    /**
     * Accessors for _reactionTime
     * get ReactionTime : return float
     * set ReactionTime : Para float
     */
    get ReactionTime()
    {
        return this._reactionTime;
    }
    set ReactionTime(reactionTime)
    {
        this._reactionTime = reactionTime;
    }
    /**
     * Accessors for _questionLoadTime
     * get QuestionLoadTime : return date
     * set QuestionLoadTime : Para date
     */
    get QuestionLoadTime()
    {
        return this._questionLoadTime;
    }

    set QuestionLoadTime(createdTime)
    {
        this._questionLoadTime = createdTime;
    }
    /**
     * Accessors for _table1
     * get Table1 : return int
     * set Table1 : Para int
     */
    get Table1()
    {
        return this._table1;
    }

    set Table1(newTable1)
    {
        this._table1 = newTable1;
    }
    /**
     * Accessors for _table2
     * get Table2 : return int
     * set Table2 : Para int
     */
    get Table2()
    {
        return this._table2;
    }

    set Table2(newTable2)
    {
        this._table2 = newTable2;
    }
    /**
     * Accessors for _mathID
     * get MathID : return int
     * set MathID : Para int
     */
    get MathID()
    {
        return this._mathID;
    }

    set MathID(newMathID)
    {
        this._mathID = newMathID;
    }

    /**
     * Accessors for _answer
     * get Answer : return int
     * set Answer : Para int
     */
    get Answer()
    {
        return this._answer;
    }
    set Answer(newAnswer)
    {
        this._answer = newAnswer;
    }

    /**
     * This function is designed to return directly the question with a format A X B
     * return string
     */
    getQuestion()
    {
        return this._table1 + " x " + this._table2;
    }
    /**
     * This function is designed to return directly the answer of the questtion with a format
     * return string
     */
    getAnswer()
    {
        return "The answer was " + this._answer;
    }
}
//Global Variables for timer use
var CCOUNT; // Count down reference
var count; // variable to count down
var t; //temp variable to put the timer on pause and holds the current timer value
var currentQuestion = 0; //index of current question
var questions = Array; // list of the questions
/**
 * This function is activated when the document is loading
 */
$(document).ready(function () {

    let numberofquestions = 24; //Initializing the number of questions
    questions = main(numberofquestions);//call the startup function and init the gamereturns the list of questions
    currentQuestion = 1; // setting the currentquestion
    CCOUNT = 5 //set the timer countdown
    cdreset(); // Reset timer
    LoadQuestion(questions,currentQuestion); //Displays the new question
    //set the time of the initial question
    questions[currentQuestion].QuestionLoadTime = Date.now();
    //On click of the start button -> Change the format when the button is clicked
    $("#startButtonGuidedMode").click(function () {
        countdown(); //Start timer
        //Display content to start the game
        $("#startButtonGuidedMode").hide("fast", "linear")
        $("#textPlayGuidedMode").hide("fast", "linear")
        $("#imgGuidedMode").hide("fast","linear")
        $("#textQuestionGuidedMode").css("display", "block")
        $("#gameTextGuidedMode").css("display", "block")
        $("#safeTimerDisplayGuidedMode").css("display","block");
    })
    //On click of the timer -> Stops the timer and displays the True or False answers
    $("#safeTimerDisplayGuidedMode").click(function(){
        cdreset(); //resets and pauses the timer
        SetReactionTime();
        displayAnswers(); //displays the correct answer
    });
    $("#buttonTrueGuidedMode").click(function()
    {
        AnswerResult(true);
        if(questions.length-1 >= currentQuestion)
        {
            currentQuestion++;
        }
        displayNextButton();
    });
    $("#buttonFalseGuidedMode").click(function()
    {
        AnswerResult(false);
        if(questions.length-1 >= currentQuestion)
        {
            currentQuestion++;
        }
        displayNextButton();
    });

    $("#buttonNextGuidedMode").click(function () {
        if(currentQuestion < questions.length)
        {
            LoadQuestion();
            //set the time of the current question
            cdreset(); //reset the timer
            countdown(); //start the count down
            FormatPage();//format the page
            //check if the next question is last
            if(currentQuestion + 1 === questions.length)
            {
                $("#buttonNextGuidedMode").text("Finish the game.");
            }
        }else
        {
            ReturnToMenu(); //return to menu
        }
    })
    $("#userStatisticsGuidedMode").click(function()
    {
        let url= document.URL;
        let split = url.split("action=")
        let playerEmail = getCookie("splitEmail") + "@" + getCookie("splitDomain");
        const newUrl = split[0] + "action=statistics&"+"email="+playerEmail;
        window.open(newUrl);
    })
    $("#buttonExitGuidedMode").click(function (){

        removeCookie("guidedmode");
    })
});


/**
 * This function is designed to set the reaction time to the question
 * Type void
 */
function SetReactionTime()
{
    questions[currentQuestion].ReactionTime = GetReactionTime(questions[currentQuestion].QuestionLoadTime);
}
/**
 * This function is designed to update the view on timer update
 */
function cddisplay() {
    //display every second
    document.getElementById('safeTimerDisplayGuidedMode').innerHTML = count + "'s";
}
//This function is designed to pause the countdown
function cdpause() {
    clearTimeout(t);// pauses countdown
}
/**
 * This function is designed to count down the timer
 * Type Void
 */
function countdown() {

    cddisplay(); //Update the view
    if (count === 0) {
        // time is up
        SetReactionTime();
        displayAnswers(); // display the answers
    } else {
        //continue to count down
        count--;
        t = setTimeout(countdown, 1000);
    }
}
/**
 * This function is designed to reset the timer back to the initialized count
 * Type Void
 */
function cdreset() {
    // resets countdown
    count = CCOUNT; //reset the count
    cdpause(); //pause the timer
    cddisplay(); //display on timer
}
/**
 *  This function is designed to display the current question
 */
function LoadQuestion() {
    $("#gameTextGuidedMode").text(questions[currentQuestion].getQuestion());
    questions[currentQuestion].QuestionLoadTime = Date.now(); //get the current day of the question
}

/**
 * This function is designed to display the next Button
 * Type Void
 */
function displayNextButton()
{
    $("#reactionTime").css("display","block");
    $("#buttonNextGuidedMode").css("display","flex");
    $("#buttonTrueGuidedMode").css("display", "none");
    $("#buttonFalseGuidedMode").css("display","none");
}
/**
 * This function is designed to display the current answer and form
 * Type Void
 */
function displayAnswers()
{
    $("#gameTextGuidedMode").text(questions[currentQuestion].getAnswer()); //displays the current answer
    $("#buttonTrueGuidedMode").css("display", "block");
    $("#buttonFalseGuidedMode").css("display", "block");
    $("#safeTimerDisplayGuidedMode").css("display","none");
}
/**
 * This function is designed to send back to the menu when the game is finished
 * Type Void
 */
function ReturnToMenu()
{
    let url= document.URL;
    let split = url.split("action=")
    const newUrl = split[0] + "action=menu";
    location.replace(newUrl);
}
/**
 * This function is designed to reformat the page
 * Type void
 */
function FormatPage()
{
    $("#buttonTrueGuidedMode").css("display","none")
    $("#buttonFalseGuidedMode").css("display","none");
    $("#buttonNextGuidedMode").css("display","none");
    $("#textQuestionGuidedMode").css("display","block");
    $("#safeTimerDisplayGuidedMode").css("display","block");
    $("#reactionTime").css("display","none");
}
/**
 * This function is designed to display the reaction time and then send question result to the update function
 * Para @truth : bool
 * Type Void
 */
function AnswerResult(truth)
{
    let reaction = questions[currentQuestion].ReactionTime; //Returns the user's reaction time
    $("#reactionTime").text("Your reaction time is " + reaction + "'s");
    UpdateRecord(questions[currentQuestion].MathID,truth,reaction);
}
/**
 * This function is designed to calculate the reaction time
 * Para @createdTime : Date
 * return float
 * */
function GetReactionTime(createdTime)
{
    clickedTime=Date.now(); //gets the current time

    return (clickedTime-createdTime)/1000; //calculates the reaction time
}

/**
 * This function is designed to update the question's result to data base by going through PHP in POST
 * para @id : int, @truth : bool, @react : float
 */
function UpdateRecord(id,truth,react)
{
    const playerEmail = getCookie("splitEmail") + "@" + getCookie("splitDomain");
    jQuery.ajax({
        type: "GET",
        url: "index.php",
        data:{
            action:"update",
            tableID:id,
            answer: truth,
            gameMode:"guidedmode",
            reactionTime:react,
            email:playerEmail,
        },
        cache: false,
        success: function(response)
        {
            console.log("The result has been saved to data base.");
        }
    });
}
/**
 * This function is designed to get a cookie by it's name
 * Para @name : string
 */
function getCookie(name) {
    const value = `; ${document.cookie}`; //get cookies from document
    const parts = value.split(`; ${name}=`); //get cookie by name
    if (parts.length === 2) return parts.pop().split(';').shift(); //Seperate the cookies to return the wanted cookie
}
/**
 * This function is designed to create an array of string that was from value that was from a cookie
 * Para @resultfromcookie : string
 * return array [string]
 */
function createArrayFromCookie(resultfromcookie) {
    return resultfromcookie.split('_');
}

/**
 * This function is the main function to call all functions that are required to start the game and returns the created questions
 * Para @numberofquestions : int
 * return array [Question]
 */
function main(numberofquestions) {
    let name = "guidedmode";
    let resultfromcookie = getCookie(name); //Get the selected check boxes from cookie

    let listcheckedboxes = createArrayFromCookie(resultfromcookie); //Create a list from the cookie
    let mathtables = listcheckedboxes.map((checkbox) => extractTableNumberFromList(checkbox)) //Transform the table of string to a list of math tables
    return createQuestions(mathtables,numberofquestions); //returns the questions created
}

/**
 * This function is designed to extract number values from a string
 * Para @item : string
 * return int
 */
function extractTableNumberFromList(item) {
    return parseInt(item.match(/\d+/)[0]); //return the number that was in the string
}
/**
 * This function is designed to create questions in function of if the questions have to be made equally or not
 * Para @mathTables : array int, numberofquestions : int
 * return array [Question]
 */
function createQuestions(mathTables,numberofquestions) {
    let check = moduloCheck(mathTables,numberofquestions);
    let eqquestions = Array;
    switch (check) {
        case true:
            eqquestions = equalQuestions(mathTables,numberofquestions) //get list of equally made questions
            break;
        case false:
            eqquestions = inequalquestions(mathTables,numberofquestions); //get a list of random questions and equally made questions
            break;
        default:
            break;
    }
    return eqquestions;
}

/**
 * This function is designed to get random questions and equally made questions to concat both lists and return all questions
 * Para @mathTables : list int, @numberofquestions : int
 * return array [Question]
 */
function inequalquestions(mathTables,numberofquestions)
{
    const remainder = moduloRemainder(numberofquestions,mathTables.length) //get the rest of the modulo
    const remained = numberofquestions-remainder;  //calculate the number of questions that need to be made equally
    const eqquestions = equalQuestions(mathTables,remained) //get equally made questions
    let otherquestions = randomQuestions(mathTables, remainder); //get random list of questions

    return eqquestions.concat(otherquestions) //Concat both lists of random questions and questions that are equally made

}
/**
 * This function is designed to return the rest if the modulo isn't 0
 * Para @numberofquestions : int, optionsLength : int
 * return int
 */
function moduloRemainder(numberofquestions,optionsLength)
{
    return numberofquestions % optionsLength;
}

/**
 * This function is designed to make generate questions equally for each math table
 * Para @mathTables : array int, @numberofquestions : int
 * return @listofquestions, array [Question]
 */
function equalQuestions(mathTables,numberofquestions) {
    //Initializing variables
    let i = 0;
    let b = 0;
    let  randomNumber = Math.floor(Math.random() * 13);
    let randomNumberIndex = [];
    let listofquestions = Array(null);
    let questionspertable = numberofquestions / mathTables.length; //calculates the number of questions per table

    //Create brut questions
    for (b = 1; b <= mathTables.length; b++) {
        for (i = 0; i < questionspertable; i++) {
            do
            {
                randomNumber = Math.floor(Math.random() * 13);
            }while(randomNumberIndex.includes(randomNumber) === true);
            randomNumberIndex.push(randomNumber); //adds the random number to an archive
            let tempindexvalue = mathTables[b-1];
            let answer = tempindexvalue * randomNumber;
            let tempQuestion = new Question();
            //set the question's properties
            tempQuestion.Table1 = tempindexvalue;
            tempQuestion.Table2 = randomNumber;
            tempQuestion.MathID = tempindexvalue;
            tempQuestion.Answer = answer;

            listofquestions.push(tempQuestion); //add the question to the list
        }
        randomNumberIndex = []; //Reset index values
    }

    return listofquestions; //return list of questions
}

/**
 * this function is designed to create random answers
 * Para @mathtables : array string, @remainder : int
 * return @listofquestions, array [Question]
 */
function randomQuestions(mathTables,remainder) {
    //Initializing variables
    let i = 0;
    let randomNumber = Math.floor(Math.random() * 13);
    let randomIndex = Math.floor(Math.random() * remainder);
    let questionAlreadyUsed = [];
    let listofquestions = [];

    //Generate the questions
    for (i = 1; i <= remainder; i++) {
        //Make random numbers and make sure that it doesn't already exist
        do {
            randomNumber = Math.floor(Math.random() * 13); //create random number for the question
            randomIndex = Math.floor(Math.random() * remainder); //create random number for the index of numbers in math tables
        } while (questionAlreadyUsed.includes(mathTables[randomIndex]) === true);

        questionAlreadyUsed.push(mathTables[randomIndex]); //Archive the random number
        let tempQuestion = new Question(); //Instance a question
        //Set question's properties
        tempQuestion.Table1 = mathTables[randomIndex];
        tempQuestion.Table2 = randomNumber;
        tempQuestion.MathID = mathTables[randomIndex];
        tempQuestion.Answer = mathTables[randomIndex] * randomNumber;

        listofquestions.push(tempQuestion); //add question to list
    }
    return listofquestions; //return list of questions
}
/**
 * This function is designed to check if the modulo of the number of questions by math tables is 0
 * para @mathtables : array int, @numberofquestions : int
 * return @truth, bool
 */
function moduloCheck(mathTables,numberofquestions) {
    let truth = false;
    let length = mathTables.length;
    if ((numberofquestions % length) === 0) {
        truth = true;
    }
    return truth;
}
/**
 * This function is designed to create a cookie
 * Type void
 */
function setCookie(name, value, days) {
    let d = new Date;
    d.setTime(d.getTime() + 24*60*60*1000*days);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}
/**
 * This function is designed to remove a cookie by name
 * para @name, string
 * Type void
 */
function removeCookie(name) { setCookie(name, '', -1); }
