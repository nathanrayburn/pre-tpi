/**
 * Author : Nathan Rayburn
 * Date : 3/18/21
 * Description : This script is designed to display the statistics page
 */
$(document).ready(function() {
    $("#userStatistics").click(function()
    {
        let url= document.URL;
        let split = url.split("action=")
        const playerEmail = getCookie("splitEmail") + "@" + getCookie("splitDomain");
        let newUrl = split[0] + "action=statistics&"+"email="+playerEmail;
        location.replace(newUrl);
    })
});
//Function is designed to get the cookie by it's name
function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
