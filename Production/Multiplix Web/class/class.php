<?php
/**
 * Title : Class Result
 * Author : Nathan Rayburn
 * Date : 3/26/2021
 * Description : This file is designed to contain classes
 */

/**
 * Class Result
 * This class is to create objects for every math table result from the data base
 */
class Result {
    // Properties
    public $mathID;
    public $true;
    public $false;
    public $total;
    public $percent;
    public $avgReaction;

    //Methods
    function set_mathID($mathID) {
        $this->mathID = $mathID;
    }
    function get_mathID() {
        return $this->mathID;
    }
    function set_true($true) {
        $this->true = $true;
    }
    function get_true() {
        return $this->true;
    }
    function set_false($false) {
        $this->false = $false;
    }
    function get_false() {
        return $this->false;
    }
    function set_total($total) {
        $this->total = $total;
    }
    function get_total() {
        return $this->total;
    }
    function set_percent($percent) {
        $this->percent = $percent;
    }
    function get_percent()
    {
        return $this->percent;
    }
    function set_avgReaction($avgReaction)
    {
        return $this->avgReaction = $avgReaction;
    }
    function get_avgReaction()
    {
        return $this -> avgReaction;
    }
}
?>