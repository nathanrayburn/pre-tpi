<?php
/**
 * Author : Nathan Rayburn
 * Date : 3/18/21
 * Description : This file is designed to navigate data between the model and view
 */

/**
 * This function is designed to update in background the user's results
 * @param $tableID, int
 * @param $answer, bool
 * @param $gameMode, string
 * @param $reactionTime, float
 * @param $email, string
 */
function update($tableID,$answer,$gameMode,$reactionTime,$email)
{
    require "model/update.php";

    if(isset($tableID) && isset($answer) && isset($gameMode) && isset($reactionTime) && isset($email)) //check if all variables are set
    {
        //Update database from question result
        updateResult($tableID,$answer,$gameMode,$reactionTime,$email);
    }
}
/**
 * This function is designed to display the statistics page
 * @param $email, string
 */
function statistics($email){

    if(isset($_SESSION["userEmail"])) {
        require "model/userStats.php";
        require "class/class.php";
        $dbResults = getUserResults($email); //Gets the user's results
        calculateStatsSeparately($dbResults); //Groups the tables and calculates the statistics that need to be calculated
        require "view/statistics.php";
    }else

    {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the login page and log the user
 * @param $loginRequest, array of inputs
 */
function login($loginRequest)
{
    if(isset($_SESSION["userEmail"]))
    {
     $_Get["action"] = "menu";
     require "view/menu.php";

    }else
    {
        if(isset($loginRequest["inputUserEmailAddress"]) && isset($loginRequest["inputUserPassword"]))
        {
            $userEmail = $loginRequest["inputUserEmailAddress"];

            $userPassword = hash("sha256", $loginRequest["inputUserPassword"]);

            require "model/userManagement.php";
            if (isLoginCorrect($userEmail, $userPassword))
            {
                unset($_SESSION["loginError"]);
                createSession($userEmail);
                $_GET["action"] = "menu";

                createCookieEmail($userEmail);

                require "view/menu.php";
            }
            else
            {
                $_SESSION["loginError"] = 1;
                $_GET["action"] = "login";
                require "view/login.php";
            }
        }
        else
        {
            $_GET["action"] = "login";
            require "view/login.php";
        }
    }
}

/**
 * This function is designed to create two cookies for email and domain to be recovered by Javascript
 * @param $userEmail, string
 */
function createCookieEmail($userEmail)
{
    //setting the name of the cookies
    $cookie_name = "splitEmail";
    $cookie_name_domain = "splitDomain";

    $array = explode("@",$userEmail); //separate e-mail and domain
    $splitMail =  $array[0]; //set the e-mail
    $splitDomain = $array[1]; //set the domain
    //set the values into the cookies
    $cookie_value = $splitMail;
    $cookie_value_domain = $splitDomain;
    //create the cookies
    createCookie($cookie_name_domain,$cookie_value_domain);
    createCookie($cookie_name,$cookie_value);

}

/**
 * This function is designed to create a cookie for the math tables to be recovered by Javascript
 * @param $cookie_name , string
 * @param $cookie_value, string
 */
function createCookie($cookie_name,$cookie_value)
{
    setcookie($cookie_name,$cookie_value,time() + (86400 * 30),"/");
}

/**
 * This function is designed to display the register page and register a new user
 * @param $arrayofinputs, array of inputs
 */
function register($arrayofinputs)
{
    if(isset($_SESSION["userEmail"]))
    {
        $_Get["action"] = "menu";
        require "view/menu.php";

    }else {

        if (isset($arrayofinputs["registerUserEmailAddress"]) && isset($arrayofinputs["registerUserPassword"]) && isset($arrayofinputs["registerRepeatUserPassword"])) {
            $userEmail = $arrayofinputs["registerUserEmailAddress"];
            $userPassword = $arrayofinputs["registerUserPassword"];
            $userRepeatPassword = $arrayofinputs["registerRepeatUserPassword"];
            if ($userPassword == $userRepeatPassword) {
                unset($_SESSION["registerError"]);
                require "model/userManagement.php";

                if (isRegisterCorrect($userEmail, $userPassword)) {
                    unset($_SESSION["registerError"]);
                    createSession($userEmail);
                    $_GET["action"] = "menu";
                    createCookieEmail($userEmail);
                    require "view/menu.php";
                } else {
                    $_SESSION["registerError"] = 1;
                    $_GET["action"] = "register";
                    require "view/register.php";
                }
            } else {
                $_SESSION["registerError"] = 1;
                $_GET["action"] = "register";
                require "view/register.php";
            }
        } else {
            unset($_SESSION["registerError"]);
            $_GET["action"] = "register";
            require "view/register.php";
        }
    }
}

/**
 * This function is designed to create a new session with the user's e-mail
 * @param $userEmail, string
 */
function createSession($userEmail){
    $_SESSION["userEmail"] = $userEmail;
}

/**
 * This function is designed to logout a user, destroys the session and sends them back to home
 */
function logout()
{
    $_SESSION = array();
    session_destroy();
    $_GET["action"] = "home";
    require "view/login.php";
}

/**
 * This function is designed to display the menu
 * Type Void
 */
function menu()
{
    if(isset($_SESSION["userEmail"]))
    {
        require "view/menu.php";
    }else
    {
        require "view/login.php";
    }

}

/**
 * This function is designed to display the options page
 * @param $arrayofinputs, array of inputs
 */
function option($arrayofinputs)
{
    if(isset($_SESSION["userEmail"]))
    {
        unset($_SESSION["optionError"]);
        if(count($arrayofinputs) < 2) //Select minimum two options
        {
            $_SESSION["optionError"] = 1;
            require "view/options.php";
        }else
        {
            if(isset($arrayofinputs["checkbox0"]) || isset($arrayofinputs["checkbox1"]) || isset($arrayofinputs["checkbox2"]) || isset($arrayofinputs["checkbox3"]) || isset($arrayofinputs["checkbox4"]) || isset($arrayofinputs["checkbox5"]) || isset($arrayofinputs["checkbox6"]) || isset($arrayofinputs["checkbox7"]) || isset($arrayofinputs["checkbox8"]) || isset($arrayofinputs["checkbox9"]) || isset($arrayofinputs["checkbox10"]) || isset($arrayofinputs["checkbox11"])  || isset($arrayofinputs["checkbox12"]) )
            {
                $templist = array(); //Create temp array
                foreach($arrayofinputs as $var_name => $value)//add the checkbox values to the temp list
                {
                    array_push($templist, $var_name);
                }
                $_SESSION["mathOptions"] = $templist; //add the list of checkbox values to session
                unset($_SESSION["optionError"]);
                $_GET["action"] = "menu";
                require "view/menu.php";
            }
        }
    }else
    {
        require "view/login.php";
    }
}

/**
 * This function is designed to display the freemode page
 * Type void
 */
function freemode()
{
    if(isset($_SESSION["userEmail"]))
    {
        if(isset($_SESSION["mathOptions"]))
        {
            $cookie_name = "freemode"; //cookie name
            $array = $_SESSION["mathOptions"]; //recover user selected tables to variable
            $cookie_value = implode("_",$array); //implode the array
            createCookie($cookie_name,$cookie_value); //create cookie for free mode
            require "view/freemode.php";
        }else{
            require  "view/options.php";
        }
    }else{
        require "view/login.php";
    }
}

/**
 * This function is designed to display the guided mode page
 * Type void
 */
function guidedmode()
{
    if(isset($_SESSION["userEmail"]))
    {
        if(isset($_SESSION["mathOptions"]))
        {
            $cookie_name = "guidedmode"; //cookie name
            $array = $_SESSION["mathOptions"]; //recover user selected tables to variable
            $cookie_value = implode("_",$array); //implode the array
            createCookie($cookie_name,$cookie_value); //create cookie
            require "view/guidedmode.php";
        }else{
            require  "view/options.php";
        }
    }else{
        require "view/login.php";
    }
}