<?php
/**
 * Author : Nathan Rayburn
 * Date : 3/18/21
 * Description : This file is designed to update the data base in the background during a launched game
 */


/**
 * This function is designed to update a user's result to data base from any game mode
 * @param $tableID, int
 * @param $answer, bool
 * @param $gameMode, string
 * @param $reactionTime, float
 * @param $email, string
 */
function updateResult($tableID,$answer,$gameMode,$reactionTime,$email)
{
    //Var Init
    $lastQueryIDResult = null;
    $queryResult = null;
    $strSep = '\'';
    $gameDate = date('Y-m-d'); //Date
    $gameTime = date('H:i:s'); //Time
    //query to insert results and get last insert ID
    $updateQuery = "INSERT INTO results(mode,answer,mathID,reactionTime,gameDate,gameTime)VALUES('".$gameMode."','".$answer."','".$tableID."','".$reactionTime."','".$gameDate."','".$gameTime."');SELECT LAST_INSERT_ID();";

    require 'dbConnector.php';
    //Execute insertion of the result and return the last insert ID
    try{
        $lastQueryIDResult = executeQueryLastID($updateQuery);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }

    $lastID = $lastQueryIDResult;

    //Query to get the user's id
    $selectUserID = "SELECT id FROM players WHERE email = ".$strSep.$email.$strSep;
    //Execution to get the user's ID
    try{
        $queryResult = executeQuery($selectUserID);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }

    //Insert into the table which sets the owner to the result
    try {
        $playerID = $queryResult[0]["id"];
        $updateQueryMidTable = "INSERT INTO players_has_results(playersID,resultsID)VALUES('".$playerID."','".$lastID."');";
        insertQuery($updateQueryMidTable);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }

}