<?php
/**
 * Author : Nathan Rayburn
 * Date : 3/18/31
 * Description : This file is used to connect to data base and execute queries
 */

/**
 * This function is designed to execute queries into data base and returns fetches the data selected
 * @param $query
 * @return array|null
 */
function executeQuery($query): ?array
{
    $queryResult = null;

    $dbConnexion = openDBConnexion();

    if($dbConnexion != null)
    {
        $statement = $dbConnexion->prepare($query);
        $statement->execute();
        $queryResult = $statement->fetchAll();
    }
    $dbConnexion = null;
    return $queryResult;
}

/**
 * Derived function to execute queries but returns the last insert ID
 * @param $query
 * @return int|null
 */
function executeQueryLastID($query): ?int
{
    $queryResult = null;

    $dbConnexion = openDBConnexion();

    if($dbConnexion != null)
    {
        $statement = $dbConnexion->prepare($query);
        $statement->execute();
        $queryResult = $dbConnexion->lastInsertId();
    }
    $dbConnexion = null;

    return $queryResult;
}
/**
 * This function is designed to insert data into data base
 * @param $query
 */
function insertQuery($query)
{

    $dbConnexion = openDBConnexion();

    if($dbConnexion != null)
    {
        $statement = $dbConnexion->prepare($query);
        $statement->execute();
    }
    $dbConnexion = null;
}

/**
 * This function is designed to use the PDO driver to connect to data base
 * @return PDO|int
 */
function openDBConnexion(): PDO|int
{
    $dbConnexion = 0;

    $sqlDriver = 'mysql';
    $dbHostname = 'web22.swisscenter.com';
    $dbPort = '3306';
    $dbCharset = 'utf8';
    $dbName = 'pretpi_nrn_multi';

    $dbUsername = 'pretpi_nrn_multi';
    $dbPassword = 'Lsof5YS_@6';

    $dbInfo = $sqlDriver.":host=".$dbHostname.";dbname=".$dbName.";port=".$dbPort.";charset=".$dbCharset;

    try
    {
        $_POST["error"] = 0;
        $dbConnexion = new PDO($dbInfo, $dbUsername, $dbPassword);
    }
    catch (PDOException $exception)
    {
        echo "Connexion failed; ".$exception->getMessage();
        $_POST["error"] = 1;
    }

    return $dbConnexion;
}













