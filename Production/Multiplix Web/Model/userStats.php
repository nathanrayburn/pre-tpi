<?php
/**
 * Author : Nathan Rayburn
 * Data : 3/18/21
 * Description : This file is designed to get user results and sort the results
 */

use JetBrains\PhpStorm\Pure;

/**
 * This function is designed to
 * @param $email, string
 * @return array, array of user results returned from data base
 */
function getUserResults($email): array
{
    //initializing variables
    $array = [];
    $queryResult = null;
    $strSep = '\'';
    //get the user's id
    //Query to get the user's id
    $selectUserIDQuery = "SELECT id FROM players WHERE email = ".$strSep.$email.$strSep;
    require 'dbConnector.php';
    //Execution to get the user's ID
    try{
        $queryResult = executeQuery($selectUserIDQuery);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }
    //get the user's game results
    try{
        $userID = $queryResult[0]["id"];
        $getUserResultsQuery = "SELECT results.id, results.reactionTime, results.gameDate, results.answer, results.mathID,results.mode FROM results INNER JOIN players_has_results ON results.id = players_has_results.resultsID WHERE '$userID' = players_has_results.playersID";
        $array = executeQuery($getUserResultsQuery);
    }catch(Exception $exception)
    {
        echo "Error : ".$exception;
    }

    return $array;

}

/**
 * This function is designed to create the user's statistics and send a list of objects type Results per game mode with calculated percentage and reaction time
 * The array is a list of answered questions that were returned by the data base
 * @param $array, array user results returned from data base
 */
function calculateStatsSeparately($array)
{

    $mathIDTableIndexFreeMode = []; //Archives existent tables that are in the free mode list
    $mathIDTableIndexGuidedMode = [];//Archives existent tables that are in the guided mode list
    $arraynewlistFreeMode = []; //List of results for free mode
    $arraynewlistGuidedMode = []; //List of results for guided mode
    //math table array will be constructed like this : mathid, total of answer, number of true answer, number of false answers
    foreach ($array as $row) {

        $boolFree = checkMathIDExists($mathIDTableIndexFreeMode, $row["mathID"]); //Checks if the table exists in the archive's list for free mode
        $boolGuided = checkMathIDExists($mathIDTableIndexGuidedMode, $row["mathID"]); //Checks if the table exists in the archive's list for free mode
        //Checks that if the table exists in either the free mode list or guided mode list
        if (($boolFree == true && $row["mode"] == "freemode") || ($boolGuided == true && $row["mode"] == "guidedmode")) {
            //Update results of the table that is existent in the free mode list
            if($boolFree == true && $row["mode"] == "freemode")
            {
                foreach ($arraynewlistFreeMode as $result) { //Looks for the created object that is in the current list
                    if ($result->get_mathID() == $row["mathID"]) { //Initializes the new result if there is none in the list

                        $result->set_total($result->get_total() + 1); //Update total answers
                        if ($row["answer"] == "true") {
                            $result->set_true($result->get_true() + 1); //Update total right answers
                        }
                        $result->set_avgReaction($result->get_avgReaction() + $row["reactionTime"]);
                        $result->set_percent(round($result->get_true() / $result->get_total() * 100)); //Calculate the percentage
                        break;
                    }
                }
            }
            //Update results of the table that is existent in the guided mode list
            if($boolGuided == true && $row["mode"] == "guidedmode")
            {
                foreach ($arraynewlistGuidedMode as $result) { //Looks for the created object that is in the current list
                    if ($result->get_mathID() == $row["mathID"]) { //Initializes the new result if there is none in the list
                        $result->set_total($result->get_total() + 1); //Update total answers
                        if ($row["answer"] == "true") {
                            $result->set_true($result->get_true() + 1); //Update total right answers
                        }
                        $result->set_avgReaction($result->get_avgReaction() + $row["reactionTime"]);
                        $result->set_percent(round($result->get_true() / $result->get_total() * 100)); //Calculate the percentage
                        break;
                    }
                }
            }
        } else {
            $tempResult = new Result(); //Instance an object type Result
            switch($row["mode"]) //Create new object depending if it's for free mode or guided mode
            {
                case "freemode":
                    //Creates a new question if the math table hasn't been created
                    $tempResult->set_mathID($row["mathID"]);
                    $tempResult->set_total($tempResult->get_total() + 1); //Update total answers
                    if ($row["answer"] == "true") {
                        $tempResult->set_true($tempResult->get_true() + 1); //Update total right answers
                        $tempResult->set_avgReaction($row["reactionTime"]);
                        $tempResult->set_percent(round($tempResult->get_true() / $tempResult->get_total() * 100)); //Calculate the percentage
                    }
                    array_push($mathIDTableIndexFreeMode, $row["mathID"]);//Add the math table to archives
                    array_push($arraynewlistFreeMode, $tempResult);//Add the object to the list
                    break;
                case "guidedmode":
                    //Creates a new question if the math table hasn't been created
                    $tempResult->set_mathID($row["mathID"]);
                    $tempResult->set_total($tempResult->get_total() + 1); //Update total answers
                    if ($row["answer"] == "true") {
                        $tempResult->set_true($tempResult->get_true() + 1); //Update total right answers
                        $tempResult->set_avgReaction($row["reactionTime"]);
                        $tempResult->set_percent(round($tempResult->get_true() / $tempResult->get_total() * 100)); //Calculate the percentage
                    }
                    array_push($mathIDTableIndexGuidedMode, $row["mathID"]); //Add the math table to archives
                    array_push($arraynewlistGuidedMode, $tempResult); //Add the object to the list
                    break;
                default :
                    break;
            }
        }

    }

    $_SESSION["statsfreemode"] = sortTable($arraynewlistFreeMode); //sets the list of results to the user's session
    $_SESSION["statsguidedmode"] = sortTable($arraynewlistGuidedMode); //sets the list of results to the user's session
}

/**
 * This function is designed to check if the math id already exists in the index array
 * @param $array, list of math tables (int)
 * @param $id , int
 * @return bool, true or false
 */
#[Pure] function checkMathIDExists($array, $id): bool
{
    if(in_array($id,$array) == true)
    {
        return true;
    }else{
        return false;
    }
}
/**
 * This function is designed to sort the results by math table ID
 * This returns a list of objects Results
 * @param $results, array of different data of every table
 * @return mixed, array of different data of every table sorter
 */
function sortTable($results): mixed
{

    $amount = count($results); //Number of math tables
    $count = 0; //counter
    //Sorts the array of math tables until it finishes the amount of math tables that have been played
    do{
        for($i = 0; $i<$amount-1;$i++)
        {
            if($results[$i]->get_mathID() > $results[$i+1]->get_mathID())
            {
                $temp = $results[$i]; //set to temp variable
                $results[$i]=$results[$i+1]; //set the next index to the current index
                $results[$i+1] = $temp; //set the next index to the old index
            }
        }
        $count ++; //increment counter
    }while($count<=$amount);

    return $results; //return the sorted list of results
}