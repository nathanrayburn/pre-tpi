<?php
/**
 * Author : Nathan Rayburn
 * Date: 3/18/21
 * Description : This file is designed to manage the user's login et register function
 */

/**
 * This function is designed to get password from data base from the entered e-mail
 * @param $userEmailAddress
 * @param $userPassword
 * @return bool, true if the data password is the same as the password that the user has entered otherwise false
 */
function isLoginCorrect($userEmailAddress, $userPassword)
{
    $isLoginCorrect = false;

    $strSep = '\'';

   $loginQuery = 'SELECT password FROM players WHERE email = '.$strSep.$userEmailAddress.$strSep;

    require 'model/dbConnector.php';

    $queryResult = executeQuery($loginQuery);

    if(count($queryResult) == 1)
    {
        $userHashedPassword = $queryResult[0]["password"];
        if($userHashedPassword == $userPassword){
            $isLoginCorrect = true;
        }
    }
    return $isLoginCorrect;
}

/**
 * This function is designed to check if the e-mail already exists in data base and then insert if not existant
 * @param $userEmailAddress
 * @param $userPassword
 * @return bool, true if no passwords has been returned by the data base with the entered e-mail by user otherwise false
 */
function isRegisterCorrect($userEmailAddress,$userPassword): bool
{
    $registerCorrect = false;
    $strSep = '\'';
    $query = $uniqueQuery = "SELECT password FROM players WHERE email = ".$strSep.$userEmailAddress.$strSep;
    require_once "model/dbConnector.php";
    $queryResult = executeQuery($query);
    if(count($queryResult) == 0)
    {
        $userHashedPassword = hash("sha256", $userPassword);
        $insertionQuery="INSERT INTO players(email,password) VALUES('".$userEmailAddress."','".$userHashedPassword."');";
        insertQuery($insertionQuery);
        $registerCorrect=true;
    }

    return $registerCorrect;
}
