<?php
/**
 * Author : Nathan Rayburn
 * Date : 03/26/2021
 * Description : This file will be the index of the whole web-site
 */
session_start();
require 'controller/controller.php';

if(isset($_GET["action"]) || isset($_POST["action"]))
{
    if(isset($_POST["action"]))
    {
        $_GET["action"] = $_POST["action"];
    }
    switch($_GET["action"])
    {
        case "menu":
            menu();
            break;
        case "login":
            login($_POST);
            break;
        case "logout":
            logout();
            break;
        case "register":
            register($_POST);
            break;
        case "option":
            option($_POST);
            break;
        case "guidedmode":
            guidedmode();
            breaK;
        case "freemode":
            freemode();
            break;
        case "statistics" :
            statistics($_GET["email"]);
            break;
        case "update":
            update($_GET["tableID"],$_GET["answer"],$_GET["gameMode"],$_GET["reactionTime"],$_GET["email"]);
            break;

        default:
            if(isset($_SESSION["userEmail"]))
            {
                menu();
            }else{
                login($_POST);
            }
            break;
    }
}
else
{
    login($_POST);
}
